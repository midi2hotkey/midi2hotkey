#!/usr/bin/env python

# Based on
# http://trac.chrisarndt.de/code/browser/projects/python-rtmidi/trunk/tests/test_midiin_callback.py
#
"""Translates MIDI commands to keystrokes

This shows how to use a MIDI controller for hotkeys"""

from evdev import uinput, ecodes as e

import rtmidi
import sys
import time

try:
    raw_input
except NameError:
    # Python 3
    raw_input = input


class MidiInputHandler(object):

    def __init__(self, port):
        self.port = port
        self._wallclock = time.time()
        self.ui = uinput.UInput()
        self.message2command = {
            # Number Pad
            64: self.sendkeys([e.KEY_KP1,]),
            65: self.sendkeys([e.KEY_KP2,]),
            66: self.sendkeys([e.KEY_KP3,]),
            48: self.sendkeys([e.KEY_KP4,]),
            49: self.sendkeys([e.KEY_KP5,]),
            50: self.sendkeys([e.KEY_KP6,]),
            32: self.sendkeys([e.KEY_KP7,]),
            33: self.sendkeys([e.KEY_KP8,]),
            34: self.sendkeys([e.KEY_KP0,]),
            67: self.sendkeys([e.KEY_KPDOT,]),
            35: self.sendkeys([e.KEY_KPSLASH,]),
            # Transport Controls
            41: self.sendkeys([e.KEY_LEFTALT, e.KEY_A]),
        }

    def __call__(self, event, data=None):
        message, deltatime = event
        self._wallclock += deltatime
        if message[2] == 127:
            self.message2command.get(message[1], lambda:None)()
        print("[%s] %r" % (self.port, message))

    def sendkeys(self, keys):
        def wrapped():
            for key in keys:
                self.ui.write(e.EV_KEY, key, 1)
            for key in sorted(keys, reverse=True):
                self.ui.write(e.EV_KEY, key, 0)
            self.ui.syn()
        return wrapped


print("Creating MidiIn object.")
midiin = rtmidi.MidiIn()

ports = midiin.get_ports(encoding='utf-8')
if not ports:
    print("No MIDI input ports found.")
    del midiin
    sys.exit(1)
else:
    print("Available MIDI input ports:\n")
    for port, name in enumerate(ports):
        print("[%i] %s" % (port, name))
    print('')

try:
    port = int(sys.argv[1])
except:
    try:
        r = raw_input("Select MIDI input port (Control-C to exit) [0]: ")
        port = int(r)
    except (KeyboardInterrupt, EOFError):
        print('')
        del midiin
        sys.exit()
    except (ValueError, TypeError):
        port = 0

if port < 0 or port >= len(ports):
    print("Invalid port number: %i" % port)
    del midiin
    sys.exit(1)
else:
    port_name = ports[port]

print("Opening MIDI input port #%i (%s)." % (port, port_name))
midiin.open_port(port)

print("Entering main loop. Press Control-C to exit.")
try:
    print("Attaching MIDI input callback handler.")
    midiin.set_callback(MidiInputHandler(port_name))
    # just wait for keyboard interrupt in main thread
    while True:
        time.sleep(1)
except uinput.UInputError, msg:
    advice = """
To use regularly you should ensure the uinput kernel module gets
loaded automatically and that your user has permission to write to it
(i.e. a udev rule)

For a quick test you can run the following commands as root (at your own risk):

`modprobe uinput && chgrp users /dev/uinput && chmod g+rw /dev/uinput`
    """
    print("\nError: {0}\n{1}".format(msg, advice))
except KeyboardInterrupt:
    print('')
finally:
    print("Exit.")
    midiin.close_port()
    del midiin
